package com.infodiegohermoso.fruitworld.models;

public class Fruit {
    String nombre;
    String origen;
    int icon;

    public Fruit()
    {}

    public Fruit(String nombre, String origen, int icon)
    {
        this.nombre = nombre;
        this.origen = origen;
        this.icon = icon;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
